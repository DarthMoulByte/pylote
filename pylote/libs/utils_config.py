# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------
# This is a part of Pylote project.
# Author:       Pascal Peter
# Copyright:    (C) 2008-2019 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------


"""
DESCRIPTION :
    La fenêtre de configuration.
"""

# importation des modules utiles :
from __future__ import division, print_function
import sys
import os

import utils, utils_functions, utils_instruments

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



###########################################################"
#   DIALOG DE CONFIGURATION
###########################################################"

class ScreenPage(QtWidgets.QWidget):
    """
    Configuration de l'écran :
        variable globale SCREEN_MODE
        variable globale SCREEN_NUMBER
    """
    def __init__(self, parent=None):
        super(ScreenPage, self).__init__(parent)
        self.main = parent.main

        # configuration de la variable globale SCREEN_MODE :
        groupBoxScreenMode = QtWidgets.QGroupBox('')
        title = QtWidgets.QApplication.translate(
            'main', 'Screen usage mode')
        titleLabel = QtWidgets.QLabel(utils_functions.u(
            '<p align="center"><b>{0}</b></p>').format(title))
        radio1 = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate('main', 'All space'))
        self.radioScreenMode = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate('main', 'Full screen'))
        if utils.SCREEN_MODE == 'FULL_SCREEN': 
            self.radioScreenMode.setChecked(True)
        else:
            radio1.setChecked(True)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(titleLabel)
        vLayout.addWidget(radio1)
        vLayout.addWidget(self.radioScreenMode)
        vLayout.addStretch(1)
        groupBoxScreenMode.setLayout(vLayout)
        bigEditorScreenMode = QtWidgets.QTextEdit()
        bigEditorScreenMode.setReadOnly(True)
        bigEditorScreenMode.setText(
            QtWidgets.QApplication.translate(
                'main', 
                ''
                '<p align=left><b>All space: </b>'
                'the application use all the free space on desktop.</p>'
                '<p></p>'
                '<p align=left><b>Full screen: </b>'
                'choose this if you ave problems with All space mode.</p>'
                '<p></p>'
                '<p align=center><b>If you change this, you need to restart application.</b></p>'))
        # configuration de la variable globale SCREEN_NUMBER :
        groupBoxScreenNumber = QtWidgets.QGroupBox('')
        title = QtWidgets.QApplication.translate('main', 'Screen number')
        titleLabel = QtWidgets.QLabel(
            utils_functions.u('<p align="center"><b>{0}</b></p>').format(title))
        self.comboBoxScreenNumber = QtWidgets.QComboBox()
        total = QtWidgets.QApplication.desktop().screenCount()
        if utils.SCREEN_NUMBER >= total:
            total = utils.SCREEN_NUMBER + 1
        for i in range(total):
            self.comboBoxScreenNumber.addItem(
                utils_functions.u('{0} {1}').format(
                    QtWidgets.QApplication.translate('main', 'Screen'), i), i)
        self.comboBoxScreenNumber.setCurrentIndex(utils.SCREEN_NUMBER)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(titleLabel)
        vLayout.addWidget(self.comboBoxScreenNumber)
        vLayout.addStretch(1)
        groupBoxScreenNumber.setLayout(vLayout)
        bigEditorScreenNumber = QtWidgets.QTextEdit()
        bigEditorScreenNumber.setReadOnly(True)
        bigEditorScreenNumber.setText(
            QtWidgets.QApplication.translate(
                'main', 
                '<P></P>'
                '<P ALIGN=LEFT>Here you can select on which screen you will to work.</P>'))

        # mise en place :
        layout = QtWidgets.QGridLayout()
        layout.addWidget(groupBoxScreenMode,        0, 0)
        layout.addWidget(bigEditorScreenMode,       0, 1)
        layout.addWidget(groupBoxScreenNumber,      1, 0)
        layout.addWidget(bigEditorScreenNumber,     1, 1)
        self.setLayout(layout)



class ToolsWindowPage(QtWidgets.QWidget):
    """
    Configuration de la fenêtre d'outils :
        iconSize
        actions affichées dans les toolsBars
    """
    def __init__(self, parent=None):
        super(ToolsWindowPage, self).__init__(parent)
        self.main = parent.main

        # configuration de la variable iconSize :
        groupBoxIconSize = QtWidgets.QGroupBox('')
        title = QtWidgets.QApplication.translate('main', 'Icon size')
        titleLabel = QtWidgets.QLabel(
            utils_functions.u(
                '<p align="center"><b>{0}</b></p>').format(title))
        integerLabel = QtWidgets.QLabel(
            QtWidgets.QApplication.translate(
                'main', 'Enter a value between {0} and {1}:').format(8, 128))
        self.integerSpinBoxIconSize = QtWidgets.QSpinBox()
        self.integerSpinBoxIconSize.setRange(8, 128)
        self.integerSpinBoxIconSize.setSingleStep(1)
        self.integerSpinBoxIconSize.setValue(
            self.main.configDict['TOLLBAR']['iconSize'])
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(integerLabel)
        hLayout.addWidget(self.integerSpinBoxIconSize)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(titleLabel)
        vLayout.addLayout(hLayout)
        groupBoxIconSize.setLayout(vLayout)

        # configuration des actions visibles dans les toolBars :
        groupBoxVisibleActions = QtWidgets.QGroupBox('')
        title = QtWidgets.QApplication.translate(
            'main', 'Visible actions')
        titleLabel = QtWidgets.QLabel(
            utils_functions.u('<p align="center"><b>{0}</b></p>').format(title))
        self.toolbarActionsList = QtWidgets.QListWidget()
        boldFont = QtGui.QFont()
        boldFont.setBold(True)
        for (menu, actions) in self.main.toolsWindow.actions['LIST']:
            title = self.main.toolsWindow.actions['TITLE'].get(menu, menu.title())
            item = QtWidgets.QListWidgetItem(utils_functions.u('{0}').format(title))
            item.setFont(boldFont)
            self.toolbarActionsList.addItem(item)
            for action in self.main.toolsWindow.actions[actions]:
                if isinstance(action, QtWidgets.QAction):
                    item = QtWidgets.QListWidgetItem(
                        action.icon(), action.text().replace('&', ''))
                    item.setData(QtCore.Qt.UserRole, action)
                    #actionName = self.main.toolsWindow.actionsState[action][0]
                    if self.main.toolsWindow.actionsState[action][1]:
                        item.setCheckState(QtCore.Qt.Checked)
                    else:
                        item.setCheckState(QtCore.Qt.Unchecked)
                    item.setData(QtCore.Qt.UserRole, action)
                    self.toolbarActionsList.addItem(item)

        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(titleLabel)
        vLayout.addWidget(self.toolbarActionsList)
        groupBoxVisibleActions.setLayout(vLayout)

        # mise en place :
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(groupBoxIconSize)
        layout.addWidget(groupBoxVisibleActions)
        self.setLayout(layout)



class KidWindowPage(QtWidgets.QWidget):
    """
    Configuration de la fenêtre d'outils :
        iconSize
        actions affichées dans les toolsBars
    """
    def __init__(self, parent=None):
        super(KidWindowPage, self).__init__(parent)
        self.main = parent.main

        # configuration de la variable iconSize :
        groupBoxIconSize = QtWidgets.QGroupBox('')
        title = QtWidgets.QApplication.translate('main', 'Icon size')
        titleLabel = QtWidgets.QLabel(utils_functions.u(
            '<p align="center"><b>{0}</b></p>').format(title))
        integerLabel = QtWidgets.QLabel(
            QtWidgets.QApplication.translate(
                'main', 'Enter a value between {0} and {1}:').format(8, 128))
        self.integerSpinBoxIconSize = QtWidgets.QSpinBox()
        self.integerSpinBoxIconSize.setRange(8, 128)
        self.integerSpinBoxIconSize.setSingleStep(1)
        self.integerSpinBoxIconSize.setValue(
            self.main.configDict['KID']['iconSize'])
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(integerLabel)
        hLayout.addWidget(self.integerSpinBoxIconSize)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(titleLabel)
        vLayout.addLayout(hLayout)
        groupBoxIconSize.setLayout(vLayout)

        # configuration des actions visibles dans les toolBars :
        groupBoxVisibleActions = QtWidgets.QGroupBox('')
        title = QtWidgets.QApplication.translate(
            'main', 'Visible actions')
        titleLabel = QtWidgets.QLabel(
            utils_functions.u('<p align="center"><b>{0}</b></p>').format(title))
        self.toolbarActionsList = QtWidgets.QListWidget()
        boldFont = QtGui.QFont()
        boldFont.setBold(True)
        for (menu, actions) in self.main.toolsWindow.actions['LIST']:
            title = self.main.toolsWindow.actions['TITLE'].get(menu, menu.title())
            item = QtWidgets.QListWidgetItem(utils_functions.u('{0}').format(title))
            item.setFont(boldFont)
            self.toolbarActionsList.addItem(item)
            for action in self.main.toolsWindow.actions[actions]:
                if isinstance(action, QtWidgets.QAction):
                    item = QtWidgets.QListWidgetItem(
                        action.icon(), action.text().replace('&', ''))
                    item.setData(QtCore.Qt.UserRole, action)
                    if self.main.toolsWindow.actionsState[action][2]:
                        item.setCheckState(QtCore.Qt.Checked)
                    else:
                        item.setCheckState(QtCore.Qt.Unchecked)
                    item.setData(QtCore.Qt.UserRole, action)
                    self.toolbarActionsList.addItem(item)

        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(titleLabel)
        vLayout.addWidget(self.toolbarActionsList)
        groupBoxVisibleActions.setLayout(vLayout)

        # mise en place :
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(groupBoxIconSize)
        layout.addWidget(groupBoxVisibleActions)
        self.setLayout(layout)



class OtherPage(QtWidgets.QWidget):
    """
    Configuration diverses
    """
    def __init__(self, parent=None):
        super(OtherPage, self).__init__(parent)
        self.main = parent.main

        # configuration de la variable screenShotDelay :
        groupBoxScreenShotDelay = QtWidgets.QGroupBox('')
        title = QtWidgets.QApplication.translate(
            'main', 'Screenshot delay')
        title2 = QtWidgets.QApplication.translate(
            'main', 'in milliseconds')
        titleLabel = QtWidgets.QLabel(
            utils_functions.u(
                '<p align="center"><b>{0}</b> ({1})</p>').format(title, title2))
        integerLabel = QtWidgets.QLabel(QtWidgets.QApplication.translate('main', 
            'Enter a value between {0} and {1}:').format(10, 10000))
        self.integerSpinBoxScreenShotDelay = QtWidgets.QSpinBox()
        self.integerSpinBoxScreenShotDelay.setRange(10, 10000)
        self.integerSpinBoxScreenShotDelay.setSingleStep(100)
        self.integerSpinBoxScreenShotDelay.setValue(
            self.main.configDict['MAIN']['screenShotDelay'])
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(integerLabel)
        hLayout.addWidget(self.integerSpinBoxScreenShotDelay)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(titleLabel)
        vLayout.addLayout(hLayout)
        vLayout.addStretch()
        groupBoxScreenShotDelay.setLayout(vLayout)

        # configuration de la variable attachDistance :
        groupBoxAttachDistance = QtWidgets.QGroupBox('')
        title = QtWidgets.QApplication.translate(
            'main', 'Attach distance')
        title2 = QtWidgets.QApplication.translate(
            'main', 'between lines or points and ruler or square')
        titleLabel = QtWidgets.QLabel(
            utils_functions.u(
                '<p align="center"><b>{0}</b> ({1})</p>').format(title, title2))
        integerLabel = QtWidgets.QLabel(QtWidgets.QApplication.translate('main', 
            'Enter a value between {0} and {1}:').format(0, 100))
        self.integerSpinBoxAttachDistance = QtWidgets.QSpinBox()
        self.integerSpinBoxAttachDistance.setRange(0, 100)
        self.integerSpinBoxAttachDistance.setSingleStep(1)
        self.integerSpinBoxAttachDistance.setValue(
            self.main.configDict['MAIN']['attachDistance'])
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(integerLabel)
        hLayout.addWidget(self.integerSpinBoxAttachDistance)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(titleLabel)
        vLayout.addLayout(hLayout)
        vLayout.addStretch()
        groupBoxAttachDistance.setLayout(vLayout)

        # configuration de l'impression :
        groupBoxPrintConfig = QtWidgets.QGroupBox('')
        title = QtWidgets.QApplication.translate(
            'main', 'Print configuration (and PDF export)')
        titleLabel = QtWidgets.QLabel(
            utils_functions.u(
                '<p align="center"><b>{0}</b></p>').format(title))
        # le mode d'impression (FullPage ou TrueSize) :
        title = QtWidgets.QApplication.translate('main', 'Print mode')
        groupBoxPrintMode = QtWidgets.QGroupBox(title)
        self.radioPrintFullPage = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate('main', 'Full page'))
        self.radioPrintTrueSize = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate('main', 'True size'))
        if self.main.configDict['MAIN']['printMode'] == 'FullPage':
            self.radioPrintFullPage.setChecked(True)
        else:
            self.radioPrintTrueSize.setChecked(True)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(self.radioPrintFullPage)
        vLayout.addWidget(self.radioPrintTrueSize)
        groupBoxPrintMode.setLayout(vLayout)
        # orientation (Portrait ou Landscape) :
        title = QtWidgets.QApplication.translate('main', 'Orientation')
        groupBoxPrintOrientation = QtWidgets.QGroupBox(title)
        self.radioPrintPortrait = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate('main', 'Portrait'))
        self.radioPrintLandscape = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate('main', 'Landscape'))
        if self.main.configDict['MAIN']['printOrientation'] == 'Portrait':
            self.radioPrintPortrait.setChecked(True)
        else:
            self.radioPrintLandscape.setChecked(True)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(self.radioPrintPortrait)
        vLayout.addWidget(self.radioPrintLandscape)
        groupBoxPrintOrientation.setLayout(vLayout)
        # on agence tout ça :
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(groupBoxPrintMode)
        hLayout.addWidget(groupBoxPrintOrientation)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(titleLabel)
        vLayout.addLayout(hLayout)
        # les explications :
        bigEditorPrintMode = QtWidgets.QTextEdit()
        bigEditorPrintMode.setReadOnly(True)
        bigEditorPrintMode.setText(
            QtWidgets.QApplication.translate(
                'main', 
                ''
                '<p align=left><b>Full page: </b>'
                'printing will be adapted to the dimensions of the page.</p>'
                '<p></p>'
                '<p align=left><b>True size: </b>'
                'the printed document comply with the dimensions of your figures.</p>'
                '<p></p>'))
        vLayout.addWidget(bigEditorPrintMode)
        vLayout.addStretch(1)
        groupBoxPrintConfig.setLayout(vLayout)

        # mise en place :
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(groupBoxScreenShotDelay)
        layout.addWidget(groupBoxAttachDistance)
        #layout.addWidget(groupBoxPrintConfig)
        self.setLayout(layout)



class ConfigurationDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None):
        super(ConfigurationDlg, self).__init__(parent)

        self.main = parent
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'Configuration'))

        self.contentsWidget = QtWidgets.QListWidget()
        self.contentsWidget.setMaximumWidth(128)
        self.contentsWidget.setMinimumWidth(128)
        self.contentsWidget.setMinimumHeight(480)
        self.contentsWidget.setSpacing(6)
        self.contentsWidget.setViewMode(QtWidgets.QListView.IconMode)
        self.contentsWidget.setIconSize(QtCore.QSize(80, 80))

        self.pagesWidget = QtWidgets.QStackedWidget()

        title = QtWidgets.QApplication.translate(
            'main', 'Screen')
        listWidgetItem = QtWidgets.QListWidgetItem(
            title, self.contentsWidget)
        listWidgetItem.setIcon(utils.doIcon('video-display'))
        listWidgetItem.setTextAlignment(QtCore.Qt.AlignHCenter)
        self.pagesWidget.addWidget(ScreenPage(self))

        title = QtWidgets.QApplication.translate(
            'main', 'Tools window')
        listWidgetItem = QtWidgets.QListWidgetItem(
            title, self.contentsWidget)
        listWidgetItem.setIcon(utils.doIcon('preferences-desktop'))
        listWidgetItem.setTextAlignment(QtCore.Qt.AlignHCenter)
        self.pagesWidget.addWidget(ToolsWindowPage(self))

        title = QtWidgets.QApplication.translate(
            'main', 'Kids')
        listWidgetItem = QtWidgets.QListWidgetItem(
            title, self.contentsWidget)
        listWidgetItem.setIcon(utils.doIcon('applications-education'))
        listWidgetItem.setTextAlignment(QtCore.Qt.AlignHCenter)
        self.pagesWidget.addWidget(KidWindowPage(self))

        title = QtWidgets.QApplication.translate(
            'main', 'Other')
        listWidgetItem = QtWidgets.QListWidgetItem(
            title, self.contentsWidget)
        listWidgetItem.setIcon(utils.doIcon('preferences-other'))
        listWidgetItem.setTextAlignment(QtCore.Qt.AlignHCenter)
        self.pagesWidget.addWidget(OtherPage(self))

        self.contentsWidget.currentItemChanged.connect(self.changePage)
        self.contentsWidget.setCurrentRow(0)

        horizontalLayout = QtWidgets.QHBoxLayout()
        horizontalLayout.addWidget(self.contentsWidget)
        horizontalLayout.addWidget(self.pagesWidget)

        buttonBox = QtWidgets.QDialogButtonBox(
            QtWidgets.QDialogButtonBox.Ok | 
            QtWidgets.QDialogButtonBox.Cancel)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)
        buttonsLayout = QtWidgets.QHBoxLayout()
        buttonsLayout.addStretch(1)
        buttonsLayout.addWidget(buttonBox)

        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addLayout(horizontalLayout)
        mainLayout.addStretch(1)
        mainLayout.addSpacing(12)
        mainLayout.addLayout(buttonsLayout)

        self.setLayout(mainLayout)
        self.setMinimumWidth(700)
        #self.setMinimumHeight(400)
        self.show()

    def changePage(self, current, previous):
        if not current:
            current = previous
        self.pagesWidget.setCurrentIndex(
            self.contentsWidget.row(current))

    def accept(self):
        """
        On met à jour les modifications avant de fermer
        """
        # page Screen :
        if self.pagesWidget.widget(0).radioScreenMode.isChecked():
            utils.changeScreenMode('FULL_SCREEN')
        else:
            utils.changeScreenMode('FULL_SPACE')
        utils.changeScreenNumber(
            self.pagesWidget.widget(0).comboBoxScreenNumber.currentIndex())

        # page ToolsWindow :
        newSize = self.pagesWidget.widget(1).integerSpinBoxIconSize.value()
        self.main.configDict['TOLLBAR']['iconSize'] = newSize
        for i in range(self.pagesWidget.widget(1).toolbarActionsList.count()):
            item = self.pagesWidget.widget(1).toolbarActionsList.item(i)
            try:
                action = item.data(QtCore.Qt.UserRole)
                if item.checkState() == QtCore.Qt.Checked:
                    self.main.toolsWindow.actionsState[action][1] = True
                else:
                    self.main.toolsWindow.actionsState[action][1] = False
            except:
                continue

        # page KidWindow :
        newSize = self.pagesWidget.widget(2).integerSpinBoxIconSize.value()
        self.main.configDict['KID']['iconSize'] = newSize
        for i in range(self.pagesWidget.widget(2).toolbarActionsList.count()):
            item = self.pagesWidget.widget(2).toolbarActionsList.item(i)
            try:
                action = item.data(QtCore.Qt.UserRole)
                if item.checkState() == QtCore.Qt.Checked:
                    self.main.toolsWindow.actionsState[action][2] = True
                else:
                    self.main.toolsWindow.actionsState[action][2] = False
            except:
                continue

        # page Other :
        self.main.configDict['MAIN'][
            'screenShotDelay'] = self.pagesWidget.widget(3).integerSpinBoxScreenShotDelay.value()
        self.main.configDict['MAIN'][
            'attachDistance'] = self.pagesWidget.widget(3).integerSpinBoxAttachDistance.value()        
        """
        if self.pagesWidget.widget(3).radioPrintFullPage.isChecked():
            self.main.configDict['MAIN']['printMode'] = 'FullPage'
        else:
            self.main.configDict['MAIN']['printMode'] = 'TrueSize'
        if self.pagesWidget.widget(3).radioPrintPortrait.isChecked():
            self.main.configDict['MAIN']['printOrientation'] = 'Portrait'
        else:
            self.main.configDict['MAIN']['printOrientation'] = 'Landscape'
        """

        # on ferme :
        QtWidgets.QDialog.accept(self)


